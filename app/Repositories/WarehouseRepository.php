<?php

namespace App\Repositories;


use App\Contracts\RepositoryContract;

/**
 * Class WarehouseRepository
 * @package App\Repositories
 */
class WarehouseRepository implements RepositoryContract {

    /**
     * @return mixed
     */
    public function all()
    {
        return app('db')->select("SELECT * FROM warehouses");
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $result = app('db')->select("SELECT * FROM warehouses WHERE id = $id LIMIT 1");

        return $result;
    }

    /**
     * @param $input
     * @return string
     */
    public function post($input)
    {
        if (isset($input['latitude']) && isset($input['longitude'])) {
            $location = json_encode([
                'latitude' => $input['latitude'],
                'longitude' => $input['longitude'],
            ]);
        }

        $query = "UPDATE warehouses SET name = '{$input['name']}'";
        $query .= isset($location) ? ", location = '{$location}'" : "";
        $query .= isset($input['manager_id']) ? ", manager_id = {$input['manager_id']}" : "";
        $query .= ", updated_at = NOW()";
        $query .= "WHERE id = {$input['id']}";
        $update = app('db')->select($query);

        return json_encode(['msg' => 'Warehouse updated']);
    }

    /**
     * @param $input
     * @return array
     */
    public function put($input)
    {
        if (isset($input['latitude']) && isset($input['longitude'])) {
            $location = json_encode([
                'latitude' => (float) $input['latitude'],
                'longitude' => (float) $input['longitude'],
            ]);
        }

        $keys = "name";
        $values = "'{$input['name']}'";

        if (isset($location)) {
            $keys .= ", location";
            $values .= ", '$location'";
        }

        if (isset($input['manager_id'])) {
            $keys .= ", manager_id";
            $values .= ", {$input['manager_id']}";
        }

        $insert = app('db')->select("INSERT INTO warehouses ($keys, created_at) VALUES ($values, NOW())");

        return ['msg' => 'Warehouse created'];
    }

    /**
     * @param $id
     * @return array
     */
    public function delete($id)
    {
        $delete = app('db')->select("DELETE FROM warehouses WHERE id = $id");

        return ['msg' => 'Warehouse deleted'];
    }

}