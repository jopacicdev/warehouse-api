<?php

namespace App\Loggers;

/**
 * Class ErrorLogger
 * @package App\Loggers
 */
class ErrorLogger {

    /**
     * @param $errorMessage
     */
    public static function log($errorMessage)
    {
        app('db')->select("INSERT INTO error_logs (message, created_at) VALUES ('{$errorMessage}', NOW())");
    }
}