<?php

namespace App\Http\Controllers;

use App\Loggers\ErrorLogger;
use App\Repositories\WarehouseRepository;
use Illuminate\Http\Request;

/**
 * Class WarehouseController
 * @package App\Http\Controllers
 */
class WarehouseController extends Controller
{
    /**
     * @var WarehouseRepository
     */
    protected $repository;

    /**
     * @param WarehouseRepository $warehouseRepository
     */
    public function __construct(WarehouseRepository $warehouseRepository)
    {
        $this->repository = $warehouseRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $response = $this->repository->all();

            return $this->respondOK($response);
        } catch(\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        try {
            $result = $this->repository->find($id);

            if (!!$result) {
                return $this->respondOK($result);
            } else {
                return $this->respondWithError('Warehouse not found', 404);
            }
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'latitude' => 'numeric|nullable|required_with:longitude',
            'longitude' => 'numeric|nullable|required_with:latitude',
            'managerId' => 'integer|nullable',
        ]);

        try {
            $response = $this->repository->put($request->input());

            return $this->respondOK($response);
        } catch(\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'name' => 'required|max:50',
            'latitude' => 'numeric|nullable|required_with:longitude',
            'longitude' => 'numeric|nullable|required_with:latitude',
            'managerId' => 'integer|nullable',
        ]);

        try {
            $response = $this->repository->post($request->input());

            return $this->respondOK($response);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $response = $this->repository->delete($id);

            return $this->respondOK($response);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * @param $result
     * @return \Illuminate\Http\JsonResponse
     */
    private function respondOK($result)
    {
        return response()->json($result, 200);
    }

    /**
     * @param $errorMessage
     * @param int $httpStatusCode
     * @return \Illuminate\Http\JsonResponse
     */
    private function respondWithError($errorMessage, $httpStatusCode = 500)
    {
        ErrorLogger::log($errorMessage);

        return response()->json(['error_message' => $errorMessage], $httpStatusCode);
    }


}
