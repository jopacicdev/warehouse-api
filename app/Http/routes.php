<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return [
        'app_version' => $app->version()
    ];
});


/**
 * Warehouse routes
 */
$app->group(['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers'], function () use ($app) {
    $app->get('warehouses', 'WarehouseController@index');
    $app->get('warehouses/{id}', 'WarehouseController@find');
    $app->put('warehouses', 'WarehouseController@create');
    $app->post('warehouses', 'WarehouseController@update');
    $app->delete('warehouses/{id}', 'WarehouseController@delete');
});