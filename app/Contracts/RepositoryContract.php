<?php

namespace App\Contracts;

/**
 * Interface RepositoryContract
 * @package App\Contracts
 */
interface RepositoryContract {

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param $input
     * @return mixed
     */
    public function put($input);

    /**
     * @param $input
     * @return mixed
     */
    public function post($input);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}