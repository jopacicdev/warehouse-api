# warehouse-api

Run: `php -S localhost:8000 -t public`

Test api to showcase work.

#### List resources

```
GET /api/v1/warehouses
```
Returns a list of resource as JSON array.

#### Find single resource
```
GET /api/v1/warehouses/{id}
```

#### Crete resource
```
PUT /api/v1/warehouses
```
Fields for `Warehouse` creation:
* `name` string(50) *required*
* `latitude` float
* `longitude` float
* `manager_id` int

#### Update resource
```
POST /api/v1/warehouses
```
Fields for `Warehouse` update:
* `id` int *required*
* `name` string(50) *required*
* `latitude` float
* `longitude` float
* `manager_id` int

#### Delete Resource
```
DELETE /api/v1/warehouses/{id}
```

## Responses
In JSON format, with HTTP status codes (200 for *success*, 422 for *validation failuer*, 404 for *resource not found*, 500 for *errors*).

## Licence

Free as in *free beer*.
